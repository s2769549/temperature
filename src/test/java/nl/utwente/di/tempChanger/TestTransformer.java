package nl.utwente.di.tempChanger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** *Test the Quoter*/

public class TestTransformer {
    @Test
    public void testBook1() throws Exception {
        Transformer transformer = new Transformer();
        double temp1 = transformer.transformation("0");
        Assertions.assertEquals(32, temp1, 0.0, "Temp of first measurement");
        double temp2 = transformer.transformation("20");
        Assertions.assertEquals(68, temp2, 0.0, "Temp of second measurement");
        double temp3 = transformer.transformation("100");
        Assertions.assertEquals(212.0, temp3, 0.0, "Temp of the third measurement");
    }
}
